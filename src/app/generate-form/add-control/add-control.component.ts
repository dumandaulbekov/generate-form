import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ControlTypes } from 'src/app/_shared/models/enums';

@Component({
  selector: 'app-add-control',
  templateUrl: './add-control.component.html',
  styleUrls: ['./add-control.component.scss']
})
export class AddControlComponent implements OnInit {
  public addControlForm: FormGroup;
  public readonly controlTypes = [
    { value: ControlTypes.text },
    { value: ControlTypes.number },
    { value: ControlTypes.date },
    { value: ControlTypes.select },
    { value: ControlTypes.checkbox },
    { value: ControlTypes.textarea },
  ];

  constructor(
    private readonly fb: FormBuilder,
    private readonly _matDialogRef: MatDialogRef<AddControlComponent>
  ) {
    this.addControlForm = fb.group({
      control: new FormControl('', [Validators.required]),
      params: new FormGroup({
        name: new FormControl('', [Validators.required]),
        label: new FormControl('', [Validators.required]),
        value: new FormControl(''),
      })
    });
  }

  ngOnInit(): void {
  }

  submit() {
    console.log(this.addControlForm.value);

    this._matDialogRef.close(this.addControlForm.value)
  }
}

import { Component, ComponentFactoryResolver, ComponentRef, Input, OnInit, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FieldBindToControlModel, FieldModel, FieldParamsModel, FormModel } from '../_shared/models/form.model';
import { ControlTypes } from '../_shared/models/enums';
import { FormService } from '../_shared/services/form.service';
import { MatDialog } from '@angular/material/dialog';
import { CustomValidators } from '../_shared/helpers/customValidators';
import { InputComponent } from './control-types/input/input.component';
import { ControlModel } from '../_shared/models/control.model';
import { TextareaComponent } from './control-types/textarea/textarea.component';
import { SelectComponent } from './control-types/select/select.component';
import { CheckboxComponent } from './control-types/checkbox/checkbox.component';
import { DateComponent } from './control-types/date/date.component';
import { FileComponent } from './control-types/file/file.component';

@Component({
  selector: 'app-generate-form',
  templateUrl: './generate-form.component.html',
  styleUrls: ['./generate-form.component.scss']
})
export class GenerateFormComponent implements OnInit {
  @Input() entryData: FormModel[] = [];
  private readonly _controlAcc: ControlModel[] = [];

  @ViewChild('formContainer', { read: ViewContainerRef }) private readonly viewFormContainerRef: ViewContainerRef;

  public readonly form: FormGroup;
  public readonly ControlTypes = ControlTypes;

  private readonly _controlChooser: any = {
    text: InputComponent,
    number: InputComponent,
    textarea: TextareaComponent,
    select: SelectComponent,
    checkbox: CheckboxComponent,
    date: DateComponent,
    file: FileComponent
  };

  constructor(
    private fb: FormBuilder,
    private readonly _formService: FormService,
    private readonly _resolver: ComponentFactoryResolver,
    private readonly _matDialog: MatDialog,
    public readonly viewContainerRef: ViewContainerRef
  ) {
    this.form = fb.group({});
    this.viewFormContainerRef = this.viewContainerRef;
  }

  ngOnInit(): void {
    this.viewFormContainerRef.clear();

    for (const form of this.entryData) {

      for (const field of form.fields) {
        if (field.control === ControlTypes.group) {
          this._setGroup(this.form, field.params);
        } else {
          this._setControl(this.form, field);
        }
      }
    }
  }

  private createComponent<T>(type: Type<T>): ComponentRef<T> {
    const factory = this._resolver.resolveComponentFactory(type);
    return this.viewFormContainerRef.createComponent(factory);
  }

  private _setGroup(group: FormGroup, params: FieldParamsModel) {
    group.addControl(params.name, new FormGroup({}));
    const newGroup = group.get(params.name) as FormGroup;

    if (!params.fields) {
      return;
    }

    for (const field of params.fields) {
      if (field.control === ControlTypes.group && field.params.fields) {
        this._setGroup(newGroup, field.params);
      } else {
        this._setControl(newGroup, field);
      }
    }
  }

  private _setControl(group: FormGroup, field: FieldModel): any {
    if (!this._controlChooser[field.control]) {
      return;
    }

    const inst = this._controlChooser[field.control];
    const controlInstance = this.createComponent(inst).instance as ControlModel;

    controlInstance.setControlType(field.control);
    controlInstance.setParams(field.params);

    if (field.validators) {
      controlInstance.setValidators(field.validators);
    }

    if (field.masks) {
      controlInstance.setMasks(field.masks);
    }

    this._controlAcc.push(controlInstance);

    group.addControl(field.params.name, controlInstance.control);
    const control = group.get(field.params.name);

    if (!control) {
      return;
    }

    if (field.params.bindToControl) {
      this._setVariable(control, field.params.bindToControl);
    }
  }

  private _getFormControl(group: FormGroup, controlName: string): FormControl {
    const control = group.get(controlName);

    if (!control) {
      for (const key in group.controls) {
        if (Object.prototype.hasOwnProperty.call(group.controls, key)) {
          const element = group.controls[key];

          if (element instanceof FormGroup) {
            return this._getFormControl(element, controlName);
          }
        }
      }
    }

    return control as FormControl;
  }


  private _setVariable(control: AbstractControl, controls: FieldBindToControlModel[]): void {
    control.valueChanges.subscribe(value => {
      for (const bindControlValues of controls) {
        const bindControl = this._getFormControl(this.form, bindControlValues.bind);

        if (!bindControl) {
          return;
        }

        if (bindControlValues.params?.ifValue) {
          for (const param of bindControlValues.params.ifValue) {
            const controlName = param.value.find(f => f === value);
            const controlInstance = this._controlAcc.find(f => bindControl === f.control);

            if (controlName === value && param.validators) {
              controlInstance?.setValidators(param.validators);
            }

            if (controlName === value && param.masks) {
              controlInstance?.setMasks(param.masks);
            }
          }
        } else {
          bindControl.setValue(value);
        }
      }
    });
  };

  submit() {
    this.form.markAllAsTouched();

    console.log(this.form);
    console.log('valid', this.form.valid);
    console.log('values', this.form.value);
  }
}

import { Component, forwardRef, OnInit, Provider } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlModel } from 'src/app/_shared/models/control.model';
import { ControlTypes } from 'src/app/_shared/models/enums';
import { FieldParamsModel, FieldValidatorModel, FieldMasksModel } from 'src/app/_shared/models/form.model';

const VALUE_ACCESSOR: Provider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => TextareaComponent),
  multi: true
};
@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  providers: [VALUE_ACCESSOR]
})
export class TextareaComponent implements ControlValueAccessor, ControlModel, OnInit {
  public readonly ControlTypes = ControlTypes;
  public readonly control = new FormControl();

  public controlType: string = '';
  public params!: FieldParamsModel;
  public validators?: FieldValidatorModel[];
  public mask?: FieldMasksModel;

  constructor() {
  }

  ngOnInit(): void {
  }

  onChangeCallback(_: any) { }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void { }

  setDisabledState(isDisabled: boolean): void { }

  onChange(val: any) {
  }

  writeValue(obj: string): void {
    this.onChange(obj);
  }

  setParams(params: FieldParamsModel) {
    this.params = params;
  }

  setControlType(control: string) {
    this.controlType = control;
  }

  setMasks(mask: FieldMasksModel) {
    this.mask = mask;
  }

  setValidators(validators: FieldValidatorModel[]) {
    this.validators = validators;
  }
}

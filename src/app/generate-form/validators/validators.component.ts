import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { CustomValidators } from 'src/app/_shared/helpers/customValidators';
import { ValidatorType } from 'src/app/_shared/models/enums';
import { FieldParamsModel, FieldValidatorModel } from 'src/app/_shared/models/form.model';

@Component({
  selector: 'app-validators',
  templateUrl: './validators.component.html',
  styleUrls: ['./validators.component.scss']
})
export class ValidatorsComponent implements OnInit {
  @Input() control!: AbstractControl;
  @Input() params!: FieldParamsModel;
  @Input('validators')
  set setValidators(validators: FieldValidatorModel[]) {
    const validatorsAcc = [];

    for (const validator of validators) {
      switch (validator.name) {
        case ValidatorType.required:
          validatorsAcc.push(Validators.required);
          break;
        case ValidatorType.maxLength:
          validatorsAcc.push(Validators.maxLength(validator.params.maxLength));
          break;
        case ValidatorType.email:
          validatorsAcc.push(Validators.email);
          break;
        case ValidatorType.pattern:
          validatorsAcc.push(CustomValidators.patternValid({
            pattern: validator.params.pattern,
            msg: validator.params.msg
          }));
          break;
      }
    }

    this.control.setValidators(validatorsAcc);
    this.control.updateValueAndValidity();
  }

  constructor() { };

  ngOnInit(): void { }
}

import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { FormModel } from "../models/form.model";
import { ControlTypes, ValidatorType } from "../models/enums";

@Injectable({
  providedIn: 'root'
})
export class FormService {
  constructor(
    // private readonly http: HttpClient
  ) { }

  getMockData() {
    const json: FormModel[] = [
      {
        title: "Title",
        fields: [
          {
            control: ControlTypes.select,
            params: {
              name: 'select-identifier',
              label: 'Тип идентификатора',
              value: [
                { option: 'ИИН' },
                { option: 'БИН' },
                { option: 'Мобильный телефон' },
              ],
              bindToControl: [
                {
                  bind: 'iin',
                  params: {
                    ifValue: [
                      {
                        value: ['ИИН', 'БИН'],
                        masks: {
                          value: ['K', 'Z', /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/],
                          placeholder: 'KZ__ ____ ____ ____ ____'
                        },
                        validators: [
                          {
                            name: ValidatorType.pattern,
                            params: {
                              pattern: /^KZ\d\d\s?\d\d\d\d\s?\d\d\d\d\s?\d\d\d\d\s?\d\d\d\d/,
                              msg: 'Некорректный формат. Требуемый формат: KZXX XXXX XXXX XXXX XXXX'
                            }
                          }
                        ]
                      },
                      {
                        value: ['Мобильный телефон'],
                        masks: {
                          value: ['+', '7', '(', /\d/, /\d/, /\d/, ')', '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/],
                          placeholder: '+7(___)___-__-__'
                        },
                        validators: [
                          {
                            name: ValidatorType.pattern,
                            params: {
                              pattern: /^\+?7\(?\d\d\d\)?-?\d\d\d-?\d\d-?\d\d/,
                              msg: 'Некорректный формат. Требуемый формат: +7(XXX)-XXX-XX-XX'
                            }
                          }
                        ]
                      },
                    ],
                  }
                },
                {
                  bind: 'param_2',
                  params: {
                    ifValue: [
                      {
                        value: ['ИИН', 'БИН'],
                        masks: {
                          value: ['K', 'Z', /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/],
                          placeholder: 'KZ__ ____ ____ ____ ____'
                        },
                        validators: [
                          {
                            name: ValidatorType.pattern,
                            params: {
                              pattern: /^KZ\d\d\s?\d\d\d\d\s?\d\d\d\d\s?\d\d\d\d\s?\d\d\d\d/,
                              msg: 'Некорректный формат. Требуемый формат: KZXX XXXX XXXX XXXX XXXX'
                            }
                          }
                        ]
                      },
                      {
                        value: ['Мобильный телефон'],
                        masks: {
                          value: ['+', '7', '(', /\d/, /\d/, /\d/, ')', '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/],
                          placeholder: '+7(___)___-__-__'
                        },
                        validators: [
                          {
                            name: ValidatorType.pattern,
                            params: {
                              pattern: /^\+?7\(?\d\d\d\)?-?\d\d\d-?\d\d-?\d\d/,
                              msg: 'Некорректный формат. Требуемый формат: +7(XXX)-XXX-XX-XX'
                            }
                          }
                        ]
                      },
                    ],
                  }
                },
                {
                  bind: 'child-2',
                  params: {
                    ifValue: [
                      {
                        value: ['ИИН', 'БИН'],
                        masks: {
                          value: ['K', 'Z', /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/],
                          placeholder: 'KZ__ ____ ____ ____ ____'
                        },
                        validators: [
                          {
                            name: ValidatorType.pattern,
                            params: {
                              pattern: /^KZ\d\d\s?\d\d\d\d\s?\d\d\d\d\s?\d\d\d\d\s?\d\d\d\d/,
                              msg: 'Некорректный формат. Требуемый формат: KZXX XXXX XXXX XXXX XXXX'
                            }
                          }
                        ]
                      },
                      {
                        value: ['Мобильный телефон'],
                        masks: {
                          value: ['+', '7', '(', /\d/, /\d/, /\d/, ')', '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/],
                          placeholder: '+7(___)___-__-__'
                        },
                        validators: [
                          {
                            name: ValidatorType.pattern,
                            params: {
                              pattern: /^\+?7\(?\d\d\d\)?-?\d\d\d-?\d\d-?\d\d/,
                              msg: 'Некорректный формат. Требуемый формат: +7(XXX)-XXX-XX-XX'
                            }
                          }
                        ]
                      },
                    ],
                  }
                }
              ]
            },
            validators: [
              { name: ValidatorType.required },
            ]
          },
          {
            control: ControlTypes.text,
            params: {
              name: "iin",
              label: 'ИИН',
            },
            validators: [
              {
                name: ValidatorType.required
              },
            ],
          },
          {
            control: "checkbox",
            params: {
              name: "confirm",
              label: 'Пользовательское соглашение',
            },
            validators: [
              {
                name: 'required'
              },
            ]
          },
          {
            control: "date",
            params: {
              name: "birthday",
              label: 'Дата рождения',
              value: '2021-09-29'
            }
          },
          {
            control: ControlTypes.file,
            params: {
              label: 'Дополнительные документы',
              name: 'file'
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.group,
            params: {
              name: 'options',
              label: 'options',
              fields: [
                {
                  control: ControlTypes.text,
                  params: {
                    label: 'param-1',
                    name: 'param_1'
                  }
                },
                {
                  control: ControlTypes.text,
                  params: {
                    label: 'param-2',
                    name: 'param_2',
                  },
                },
                {
                  control: ControlTypes.group,
                  params: {
                    label: 'child-1',
                    name: 'child-1',
                    fields: [
                      {
                        control: ControlTypes.text,
                        params: {
                          label: 'child-11',
                          name: 'child-11',
                        }
                      },
                      {
                        control: ControlTypes.text,
                        params: {
                          label: 'child-2',
                          name: 'child-2',
                        }
                      }
                    ]
                  }
                }
              ]
            }
          },
        ]
      }
    ];

    return json;
  }

  getDataIqala() {
    const json: FormModel[] = [
      {
        title: '«IQALA» КАКОКОВ ДАМИР АЛИХАНОВИЧ > Оказание устных и письменных консультаций (IQALA)',
        fields: [
          {
            control: ControlTypes.select,
            params: {
              name: 'identifier-type',
              label: 'Тип заявителя',
              value: [
                { option: 'Физ.лицо' },
                { option: 'Индивидуальный предприниматель' },
                { option: 'Частное лицо' },
                { option: 'Частный предприниматель' },
              ]
            },
            validators: [
              { name: ValidatorType.required }
            ],
          },
        ]
      },
      {
        title: 'Информация о заявителе',
        fields: [
          // IIN
          {
            control: ControlTypes.number,
            params: {
              name: "iin",
              label: 'ИИН',
            },
            validators: [
              {
                name: ValidatorType.required
              }, {
                name: ValidatorType.maxLength,
                params: {
                  maxLength: 12
                }
              }
            ],
          },
          // NAME
          {
            control: ControlTypes.text,
            params: {
              name: "name",
              label: 'Имя',
            },
            validators: [
              {
                name: ValidatorType.required
              }
            ],
          },
          // SURNAME
          {
            control: ControlTypes.text,
            params: {
              name: "surname",
              label: 'Фамилия',
            },
            validators: [
              {
                name: ValidatorType.required
              },
            ],
          },
          // MIDDLE NAME
          {
            control: ControlTypes.text,
            params: {
              name: "middle-name",
              label: 'Отчество',
            },
          },
          // BIRTHDAY
          {
            control: ControlTypes.date,
            params: {
              name: "birthday",
              label: 'Дата рождения',
            },
            validators: [
              {
                name: ValidatorType.required
              },
            ],
          },
        ]
      },
      {
        title: 'Документ, удостоверяющий личность',
        fields: [
          {
            control: ControlTypes.select,
            params: {
              name: 'document-title',
              label: 'Наименование документа',
              value: [
                { option: 'ПАСПОРТ РК' },
                { option: 'УДОСТОВЕРЕНИЕ РК' },
              ]
            },
            validators: [
              { name: ValidatorType.required }
            ],
          },
          {
            control: ControlTypes.number,
            params: {
              name: 'document-number',
              label: 'Номер документа',
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.date,
            params: {
              name: 'document-date-issue',
              label: 'Дата выдачи документа',
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.text,
            params: {
              name: 'document-who-issued',
              label: 'Кем выдан документ',
              value: 'МИНИСТЕРСТВО ВНУТРЕННИХ ДЕЛ РК'
            },
            validators: [
              { name: ValidatorType.required }
            ]
          }
        ],
      },
      {
        title: 'Адрес заявителя',
        fields: [
          {
            control: ControlTypes.text,
            params: {
              name: 'city',
              label: 'Город/Населенный пункт'
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.text,
            params: {
              name: 'street',
              label: 'Улица'
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.text,
            params: {
              name: 'home',
              label: 'Дом №'
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.text,
            params: {
              name: 'apartment',
              label: 'Квартира №'
            },
            validators: [
              { name: ValidatorType.required }
            ]
          }
        ]
      },
      {
        title: 'Контакты заявителя',
        fields: [
          {
            control: ControlTypes.text,
            params: {
              name: 'email',
              label: 'Email',
            },
            validators: [
              { name: ValidatorType.required },
              { name: ValidatorType.email },
            ]
          },
          {
            control: ControlTypes.number,
            params: {
              label: 'Телефон',
              name: 'work-number'
            },
            validators: [
              { name: ValidatorType.required },
              {
                name: ValidatorType.pattern,
                params: {
                  pattern: /^[0-9]+$/,
                  msg: 'Доступен только ввод цифр'
                }
              }
            ]
          },
          {
            control: ControlTypes.number,
            params: {
              label: 'Мобильный телефон',
              name: 'phone-number'
            },
            validators: [
              { name: ValidatorType.required },
              {
                name: ValidatorType.pattern,
                params: {
                  pattern: '^[0-9]+$',
                  msg: 'Доступен только ввод цифр'
                }
              }
            ]
          }
        ]
      },
      {
        title: '-',
        fields: [
          {
            control: ControlTypes.textarea,
            params: {
              label: "Текст заявления",
              name: 'statement-text'
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.file,
            params: {
              label: 'Дополнительные документы',
              name: 'file'
            },
            validators: [
              { name: ValidatorType.required }
            ]
          }
        ]
      },
    ];

    return json;
  }

  getDataAstanaenergosbyt() {
    const json: FormModel[] = [
      {
        title: 'ТОО «Астанаэнергосбыт» Зинкевич Александр Викторович > Заключение договора на электроснабжение для юридических лиц, для не бытового потребления (в том числе и физические лица без образования ИП) на вновь подключаемый объект',
        fields: [
          {
            control: ControlTypes.select,
            params: {
              name: 'select-identifier',
              label: 'Тип идентификатора',
              value: [
                { option: 'ИИН' },
                { option: 'БИН' },
                { option: 'Мобильный телефон' },
              ],
              bindToControl: [
                {
                  bind: 'identifier',
                  params: {
                    ifValue: [
                      {
                        value: ['ИИН', 'БИН'],
                        masks: {
                          value: ['K', 'Z', /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/, ' ',
                            /\d/, /\d/, /\d/, /\d/],
                          placeholder: 'KZ__ ____ ____ ____ ____'
                        },
                        validators: [
                          {
                            name: ValidatorType.pattern,
                            params: {
                              pattern: /^KZ\d\d\s?\d\d\d\d\s?\d\d\d\d\s?\d\d\d\d\s?\d\d\d\d/,
                              msg: 'Некорректный формат. Требуемый формат: KZXX XXXX XXXX XXXX XXXX'
                            }
                          }
                        ]
                      },
                      {
                        value: ['Мобильный телефон'],
                        masks: {
                          value: ['+', '7', '(', /\d/, /\d/, /\d/, ')', '-', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/],
                          placeholder: '+7(___)___-__-__'
                        },
                        validators: [
                          {
                            name: ValidatorType.pattern,
                            params: {
                              pattern: /^\+?7\(?\d\d\d\)?-?\d\d\d-?\d\d-?\d\d/,
                              msg: 'Некорректный формат. Требуемый формат: +7(XXX)-XXX-XX-XX'
                            }
                          }
                        ],
                      },
                    ]
                  }
                }
              ]
            },
            validators: [
              { name: ValidatorType.required }
            ],
          },
          {
            control: ControlTypes.text,
            params: {
              label: "Идентификатор",
              name: "identifier",
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.select,
            params: {
              label: "Дата",
              name: "date",
              value: [
                { option: '07.07.2021' },
                { option: '07.08.2021' },
                { option: '07.09.2021' },
                { option: '07.10.2021' },
                { option: '07.11.2021' },
                { option: '07.12.2021' },
              ]
            },
            validators: [
              { name: ValidatorType.required }
            ]
          },
          {
            control: ControlTypes.select,
            params: {
              label: "Промежуток времени",
              name: "time-interval",
              value: [
                { option: '13:30 - 14:00' },
                { option: '14:30 - 15:00' },
                { option: '15:30 - 16:00' },
                { option: '16:30 - 17:00' },
                { option: '17:30 - 18:00' },
                { option: '18:30 - 19:00' },
              ]
            },
            validators: [
              { name: ValidatorType.required }
            ]
          }
        ]
      }
    ];

    return json;
  }

  getGroupData() {
    const json: FormModel[] = [
      {
        fields: [
          {
            control: 'group',
            params: {
              label: 'group',
              name: 'group',
              fields: [
                {
                  control: 'title',
                  params: {
                    label: 'title',
                    name: 'title'
                  }
                }, {
                  control: 'title',
                  params: {
                    label: 'title',
                    name: 'title'
                  }
                }
              ]
            }
          }
        ]
      }
    ];

    return json;
  }
}


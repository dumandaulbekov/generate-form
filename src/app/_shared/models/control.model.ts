import { FormControl } from "@angular/forms";
import { FieldMasksModel, FieldParamsModel, FieldValidatorModel } from "./form.model";

export interface ControlModel {
  control: FormControl;
  setParams(param: FieldParamsModel): any;
  setControlType(control: string): any;
  setValidators(validators: FieldValidatorModel[]): any;
  setMasks(masks: FieldMasksModel | null): any;
}
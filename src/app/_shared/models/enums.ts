export enum ValidatorType {
  required = 'required',
  maxLength = 'maxLength',
  minLength = 'minLength',
  pattern = 'pattern',
  email = 'email',
  mask_phone = 'phone'
}

export enum ControlTypes {
  group = 'group',
  text = 'text',
  number = 'number',
  date = 'date',
  select = 'select',
  checkbox = 'checkbox',
  textarea = 'textarea',
  file = 'file',
}
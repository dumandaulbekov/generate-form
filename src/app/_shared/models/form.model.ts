export interface FormModel {
  title?: string,
  fields: FieldModel[];
}

export interface FieldModel {
  control: string;
  params: FieldParamsModel;
  masks?: FieldMasksModel;
  validators?: FieldValidatorModel[];
}

export interface FieldParamsModel {
  name: string,
  label: string;
  value?: any;
  fields?: FieldModel[];
  bindToControl?: FieldBindToControlModel[];
  disabled?: boolean;
  readonly?: boolean;
}

export interface FieldBindToControlModel {
  bind: string,
  params?: {
    ifValue?: BindControlIfValueParams[];
  };
}

interface BindControlIfValueParams {
  value: string[],
  validators?: FieldValidatorModel[];
  masks?: FieldMasksModel,
}
export interface FieldMasksModel {
  value: (string | RegExp)[];
  placeholder: string;
}

export interface FieldValidatorModel {
  name: string;
  params?: any;

}
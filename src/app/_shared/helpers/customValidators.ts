import { AbstractControl, FormControl, Validators } from "@angular/forms";
import { ValidatorType } from "../models/enums";

export class CustomValidators {
  public static patternValid(config: any): any {
    return (control: FormControl) => {
      const pattern = new RegExp(config.pattern);

      if (control.value && !pattern.test(control.value)) {
        return {
          invalidMsg: config.msg
        };
      } else {
        return null;
      }
    };
  }
}

import { Component } from '@angular/core';
import { FormModel } from './_shared/models/form.model';
import { FormService } from './_shared/services/form.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'generateform';
  public entryData: FormModel[];

  constructor(private _formService: FormService) {
    // this.entryData = this._formService.getMockData()
    // this.entryData = this._formService.getDataIqala();
    this.entryData = this._formService.getDataAstanaenergosbyt();
  }
}

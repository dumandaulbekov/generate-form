import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GenerateFormComponent } from './generate-form/generate-form.component';
import { AddControlComponent } from './generate-form/add-control/add-control.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// CONTROL TYPE COMPONENTS
import { InputComponent } from './generate-form/control-types/input/input.component';
import { TextareaComponent } from './generate-form/control-types/textarea/textarea.component';
import { SelectComponent } from './generate-form/control-types/select/select.component';

// MATERIAL MODULES START
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { ValidatorsComponent } from './generate-form/validators/validators.component';
// MATERIAL MODULES END

// OTHER LIBLALIES 
import { TextMaskModule } from 'angular2-text-mask';
import { CheckboxComponent } from './generate-form/control-types/checkbox/checkbox.component';
import { DateComponent } from './generate-form/control-types/date/date.component';
import { FileComponent } from './generate-form/control-types/file/file.component';

@NgModule({
  declarations: [
    AppComponent,
    GenerateFormComponent,
    AddControlComponent,
    InputComponent,
    TextareaComponent,
    ValidatorsComponent,
    SelectComponent,
    CheckboxComponent,
    DateComponent,
    FileComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,

    // MATERIAL MODULES START
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
    // MATERIAL MODULES END

    // OTHER MODULES
    TextMaskModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
